(function()
{
    let search = document.querySelector('#search');
    let menu = document.querySelector('#menu');
    let form = document.querySelector('#form');
    let navbar = document.querySelector('#navbar');

    search.addEventListener('click', function(e)
    {
        form.classList.toggle('show');
        navbar.classList.remove('show');
    });

    menu.addEventListener('click', function(e)
    {
        navbar.classList.toggle('show');
        form.classList.remove('show');
    });


    // Desktop version Event
    let search_desktop = document.querySelector('#search__desktop');
    let navbar__desktop = document.querySelector('#navbar__desktop');
    search_desktop.addEventListener('click', function(e)
    {
        navbar__desktop.classList.toggle('event');
    });

    // INTRO Add >
    let onglet = document.querySelector('#onglet');
    let items = onglet.children;
    
    for(let item of items) 
    {
        if( item.children.length > 0 )
        {
            // create a span element
            let spanElement = document.createElement('span');
            spanElement.textContent= ">";
            // insert element
            item.append(spanElement);
        }
    }

    // CALC RESIZE PROGRESS-ITEM
    let progress_bar= document.querySelector('#progress-bar');
    let uls = progress_bar.querySelectorAll('ul');

    uls.forEach(ul =>{
        let width = ul.clientWidth || ul.offsetWidth;
        let lis = ul.querySelectorAll('li');
        lis.forEach(li =>{
            // ADD active CLASS
            li.addEventListener('click', function(e)
            {
                // REMOVE active CLASS ON PROGRESS-ITEM
                uls.forEach(function(ul)
                {
                    lis= ul.querySelectorAll('li');
                    lis.forEach(el => {
                        el.classList.remove('active');
                    });
                });
                // ADD active CLASS ON THE CURRENT PROGRESS-ITEM
                li.classList.add('active');
            });
            
        });
    });

    // DISPLAY HIDE CONTENT SECTION BODY
    let sections_title = document.querySelectorAll('.main_content .section_header');
    
    sections_title.forEach(section =>{
        section.addEventListener('click', function(e)
        {
            // ADD show CLASS TO THE CURRENT SECTION BODY
            let current_section = this.parentNode
            current_section.classList.toggle('event');
        });
    });

    let chapters = document.querySelector('#chapters').querySelectorAll('li');
    chapters.forEach(function(chapter)
    {
        if( !chapter.classList.contains('disabled') )
        {
            chapter.addEventListener("click", function(e)
            {
                chapters.forEach(c =>{
                    c.classList.remove('active');
                })
                chapter.classList.add('active');
            });
        }
    });

    document.querySelectorAll('.main_content p').forEach( p =>{
        p.addEventListener('mouseover', function(e)
        {
            e.target.classList.add('hover');
        })
        p.addEventListener('mouseout', function(e)
        {
            e.target.classList.remove('hover');
        })
        p.addEventListener('click', function(e)
        {
            if (document.selection) { 
                var range = document.body.createTextRange();
                range.moveToElementText(p);
                range.select().createTextRange();            
            } else if (window.getSelection) {
                var range = document.createRange();
                 range.selectNode(p);
                 window.getSelection().addRange(range);
                 document.execCommand("copy");
            }
                       
        });
    })
    document.querySelectorAll('.main_content li').forEach(li=>{
        li.addEventListener('mouseover', function(e)
        {
            e.target.classList.add('hover');
        })
        li.addEventListener('mouseout', function(e)
        {
            e.target.classList.remove('hover');
        })
        li.addEventListener('click', function(e)
        {
            if (document.selection) { 
                var range = document.body.createTextRange();
                range.moveToElementText(li);
                range.select().createTextRange();            
            } else if (window.getSelection) {
                var range = document.createRange();
                 range.selectNode(li);
                 window.getSelection().addRange(range);
                 document.execCommand("copy");
            }
                       
        });
    })

    /* SHOW HIDE FOOTER section_body WITH JQUERY */
    $(function()
    {
        let footer_sections_title = $('.footer .section_header');
        
        footer_sections_title.click( function(e)
        {
            if( $(this).siblings(".footer #section_body").is(":hidden") )
            {
                $(this).siblings(".footer #section_body").show(300);
            }
            else
            {
                $(this).siblings(".footer #section_body").hide(300);
            }
            // rotate arrow
            $(this).parent().toggleClass('event');
        });

        // disabled reload page on click of disabled element
        $('.disabled').click(function(e)
        {
            e.preventDefault();
        })
    });

    

})();