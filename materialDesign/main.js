jQuery(function($){
    
    $('.field-input').focus(function(){
        //$(this).parent().siblings().removeClass('is-focused');
        $(this).parent().addClass('is-focused has-label');
    })
    $('.field-input').blur(function(){
        let $parent  = $(this).parent();
        if($(this).val() == "")
        {
            $parent.removeClass('has-label');
        }
        $parent.removeClass('is-focused');
        //$(this).parent().addClass('is-focused');
    })
   
})
