(function(){
    var body = document.querySelector("body");
    var burger_btn = document.querySelector(".burger-btn");

    burger_btn.addEventListener('click', function(e){
        e.preventDefault();
        body.classList.toggle("display-all-topbar")
    });
})()